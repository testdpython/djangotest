"""core URL Configuration

The `urlpatterns` list routes URLs to models. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function models
    1. Add an import:  from my_app import models
    2. Add a URL to urlpatterns:  url(r'^$', models.home, name='home')
Class-based models
    1. Add an import:  from other_app.models import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
]
