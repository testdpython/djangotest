# Teste Django

O desafio é criar três classes que se relacionam entre si (por exemplo: Candidato, Endereço e Disciplina) e configurar essas classes com o Admin do Django. Você deve inserir dados por meio da interface Admin.

# Extra

A criação de Autenticação sem o Admin do Django e apresentação de dados usando o modelo MTV (“model”, “template” e "view") com Class-based views do django será considerado extra.


## 1. Clone o projeto. 

Crie o branch com seu nome+sobrenome a partir do branch master, faça o checkout nesse novo branch para realizar sua atividade.
Após conluir a questão, faça o commit e o push no branch (nome+sobrenome) criado.
	
	git clone https://gitlab.com/robertomorati/djangotest.git

## 2. Instalar dependências e Criar Ambiente Virtual

	# execute como root
	sudo apt-get update
	sudo xargs apt-get install -y < linux.txt
	sudo apt-get install git
	
	# criando ambiente virual
	python3 -m venv /home/<user>/env_test
	
	# ativando ambiente virtual
	source /home/<user>/env_test/bin/activate
	
	# instalando dependências no ambiente virtual
	pip install --upgrade pip
	pip3 install -r requirements.txt
	
## 3. Tutorial Django para Apoio ao Teste

Model em django é um tipo especial de objeto salvo na base de dados. Nós vamos usar o SQLlite database para salvar nossos dados - isso é o adaptador default do Django.
	
Em models.py nós definimos Classes que são chamadas de Models – esse é o lugar onde você deverá definir suas classes.
	
	# file models.py
	from django.db import models
	
	#example
	class Name_Class(models.Model):
		titulo = models.CharField(max_length=200)
		relacao = models.ForeignKey('Other')
		
		def __str__(self):
        		return self.title
        		
    class Other(models.Model):
    		pass
        	
    #algumas definições de tipos	
    models.CharField – this is how you define text with a limited number of characters.
    models.TextField – this is for long text without a limit. Sounds ideal for blog post content, right?
    models.DateTimeField – this is a date and time.
    models.ForeignKey – this is a link to another model.
    models. FloatField - this is a float.
    
    # configurando o admin.py
    # para fazer nosso modelo visível no Admin do Django, precisamos de:
    
    class Name_ClassAdmin(admin.ModelAdmin):
    pass
    
    
    admin.site.register(Name_Class, Name_ClassAdmin)
    
    # atualizando o modelo de dados
    
    ./manage.py makemigrations core
    
    ./manage.py migrate
    
    
    #lembre-se para acessar o admin do django você precisa de um usuário, faça:
    ./manage.py createsuperuser